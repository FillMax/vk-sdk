package com.example.testnavigation.fragments.photo

interface PhotoFragmentInjector {

    fun inject(fragment: PhotoFragment)
}