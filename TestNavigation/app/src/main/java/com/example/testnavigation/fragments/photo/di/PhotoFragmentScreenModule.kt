package com.example.testnavigation.fragments.photo.di

import com.example.testnavigation.fragments.photo.PhotoFragmentPresenter
import com.example.testnavigation.fragments.photo.PhotoFragmentPresenterImpl
import com.example.testnavigation.fragments.photo.PhotoFragmentView
import com.example.testnavigation.scope.FragmentScope
import dagger.Module
import dagger.Provides

@Module
class PhotoFragmentScreenModule(private val view: PhotoFragmentView) {

    @Provides
    @FragmentScope
    fun provideView(): PhotoFragmentView = view

    @Provides
    @FragmentScope
    fun providePresenter(presenter: PhotoFragmentPresenterImpl): PhotoFragmentPresenter = presenter

}