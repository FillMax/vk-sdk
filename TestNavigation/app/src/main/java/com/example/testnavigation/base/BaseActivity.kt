package com.example.testnavigation.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import com.example.testnavigation.app.App
import com.vk.api.sdk.VK

abstract class BaseActivity : AppCompatActivity() {
    protected abstract fun setupComponent()

    protected val app: App
        get() = application as App

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        VK.initialize(applicationContext)
        setupComponent()
    }
}
