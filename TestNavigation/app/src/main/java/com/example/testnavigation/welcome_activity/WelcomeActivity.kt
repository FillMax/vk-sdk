package com.example.testnavigation.welcome_activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.example.testnavigation.MainActivity
import com.example.testnavigation.R
import com.example.testnavigation.base.BaseActivity
import com.example.testnavigation.welcome_activity.di.WelcomeActivityScreenComponent
import com.example.testnavigation.welcome_activity.di.WelcomeActivityScreenModule
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKScope
import kotlinx.android.synthetic.main.activity_welcome.*
import javax.inject.Inject
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback


class WelcomeActivity : BaseActivity(),
    WelcomeActivityView {

    private lateinit var component: WelcomeActivityScreenComponent

    @Inject
    lateinit var presenter: WelcomeActivityPresenter

    override fun setupComponent() {
        component = app.get(this).getAppComponent().plus(WelcomeActivityScreenModule(this))
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        //Отпечаток сертификата
        /*val fingerprints = VKUtils.getCertificateFingerprint(this, this.packageName)
        Log.e(TAG, "finger ${fingerprints?.get(0)}")*/

        btnEnter.setOnClickListener {
            VK.login(this, arrayListOf(VKScope.FRIENDS))
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val callback = object : VKAuthCallback {
            override fun onLogin(token: VKAccessToken) {
                presenter.welcome(true)
                finish()
            }

            override fun onLoginFailed(errorCode: Int) {
                presenter.welcome(false)
            }
        }
        if (!VK.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun start() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun error(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }
}
