package com.example.testnavigation.fragments.friends.adapter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.testnavigation.R
import com.example.testnavigation.fragments.friends.model.VKUser
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.net.URL

abstract class FriendsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    abstract fun userClicked(userId: Int)
    abstract fun userClickedUrl(userUrl: String)
    private val friends: MutableList<VKUser> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder = UserHolder(parent.context)
        viewHolder.itemView.setOnClickListener {
            //пока оставить, вдруг пригодится
            userClicked(friends[viewHolder.adapterPosition].id)
            userClickedUrl(friends[viewHolder.adapterPosition].photo)
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as UserHolder).bind(friends[position])
    }

    fun setData(friends: List<VKUser>) {
        this.friends.clear()
        this.friends.addAll(friends)
        notifyDataSetChanged()
    }

    override fun getItemCount() = friends.size

    class UserHolder(context: Context?) : RecyclerView.ViewHolder(
        LayoutInflater.from(context).inflate(R.layout.item_friend, null)
    ) {
        private val nameTV: TextView = itemView.findViewById(R.id.itemName)
        private val avatar: ImageView = itemView.findViewById(R.id.list_avatar)

        companion object {
            private val imagesCache = arrayListOf<CacheInfo>()

            data class CacheInfo(val userId: Int, val image: Bitmap)
        }

        fun bind(user: VKUser) {
            nameTV.text = "${user.firstName} ${user.lastName}"
            if (TextUtils.isEmpty(user.photo)) {
                avatar.setImageResource(R.drawable.man)
            } else {
                val filteredCache = imagesCache.filter { it.userId == user.id }
                if (filteredCache.isNotEmpty()) {
                    avatar.setImageBitmap(filteredCache[0].image)
                } else {
                    downloadOne(user.photo)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeBy(
                            onNext = { bitmap ->
                                imagesCache.add(CacheInfo(user.id, bitmap))
                                avatar.setImageBitmap(bitmap)
                            },
                            onError = {
                                it.printStackTrace()
                            }
                        )
                }
            }

        }

        private fun downloadOne(url: String): Observable<Bitmap> {

            return Observable.create<Bitmap> { emitter ->
                try {
                    val inputStream = URL(url).openStream()
                    emitter.onNext(BitmapFactory.decodeStream(inputStream))
                } catch (e: Exception) {
                    e.printStackTrace()
                    emitter.onError(e)
                }
            }
        }

        fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
            itemView.setOnClickListener {
                event.invoke(adapterPosition, itemViewType)
            }
            return this
        }
    }
}
