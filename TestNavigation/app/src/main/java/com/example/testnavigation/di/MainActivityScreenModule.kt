package com.example.testnavigation.di

import com.example.testnavigation.*
import com.example.testnavigation.scope.ActivityScope
import dagger.Module
import dagger.Provides

@Module
class MainActivityScreenModule(private val view: MainActivityView) {

    @Provides
    @ActivityScope
    fun provideView(): MainActivityView = view

    @Provides
    @ActivityScope
    fun providepresenter(presenter: MainActivityPresenterImpl): MainActivityPresenter = presenter

}