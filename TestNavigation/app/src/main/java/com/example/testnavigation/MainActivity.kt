package com.example.testnavigation

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle

import android.view.MenuItem
import com.example.testnavigation.base.BaseActivity
import com.example.testnavigation.di.MainActivityScreenComponent
import com.example.testnavigation.di.MainActivityScreenModule
import com.example.testnavigation.fragments.friends.FriendFragment
import com.example.testnavigation.fragments.friends.FriendFragmentInjector
import com.example.testnavigation.fragments.friends.di.FriendFragmentScreenModule
import com.example.testnavigation.fragments.photo.PhotoFragment
import com.example.testnavigation.fragments.photo.PhotoFragmentInjector
import com.example.testnavigation.fragments.photo.di.PhotoFragmentScreenModule
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(),
    MainActivityView,
    FriendFragmentInjector,
    PhotoFragmentInjector,
    NavigationView.OnNavigationItemSelectedListener {

    @Inject
    lateinit var presenter: MainActivityPresenter

    private lateinit var component: MainActivityScreenComponent

    override fun setupComponent() {
        component = app.get(this).getAppComponent().plus(MainActivityScreenModule(this))

        component.inject(this)
    }

    override fun inject(fragment: FriendFragment) {
        component.plusAccount(FriendFragmentScreenModule(fragment)).inject(fragment)
    }

    override fun inject(fragment: PhotoFragment) {
        component.plusSetting(PhotoFragmentScreenModule(fragment)).inject(fragment)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(this, drawer_layout, R.string.open, R.string.close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)

        replaceFragment(FriendFragment())
        title = "Друзья"

    }


    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.nav_account -> {
                presenter.account()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun showFriend() {
        openListFriend()
    }

    private fun openListFriend() {
        val fragment = FriendFragment()
        replaceFragment(fragment)
    }

    fun openPhoto(url: String) {
        val fragment = PhotoFragment.newInstance(url)
        replaceFragment(fragment)
    }

    private fun replaceFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.conteiner, fragment)
            .addToBackStack(null)
        fragmentTransaction.commit()
    }

    override fun showFoto() {
        openListFriend()
    }
}
