package com.example.testnavigation.welcome_activity


interface WelcomeActivityPresenter {
    fun welcome(isAccess: Boolean)
}