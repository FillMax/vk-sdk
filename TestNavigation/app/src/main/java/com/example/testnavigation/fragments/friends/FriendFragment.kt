package com.example.testnavigation.fragments.friends


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.testnavigation.MainActivity
import com.example.testnavigation.R
import com.example.testnavigation.base.BaseFragment
import com.example.testnavigation.fragments.friends.adapter.FriendsAdapter
import com.example.testnavigation.fragments.friends.model.VKUser
import com.example.testnavigation.fragments.photo.PhotoFragment
import kotlinx.android.synthetic.main.fragment_account.*
import javax.inject.Inject


class FriendFragment : BaseFragment(),
    FriendFragmentView {

    @Inject
    lateinit var presenter: FriendFragmentPresenter


    private lateinit var adapter: FriendsAdapter
    var fragment = PhotoFragment()

    override fun setupComponent() {
        (activity as FriendFragmentInjector).inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        adapter = object : FriendsAdapter() {
            override fun userClicked(userId: Int) {

            }

            override fun userClickedUrl(userUrl: String) {
                (activity as? MainActivity)?.openPhoto(userUrl)
            }
        }
        friendRecyclerView.adapter = adapter

        presenter.getListFriend()
    }

    override fun showData(friendList: ArrayList<VKUser>) {
        friendRecyclerView.layoutManager = LinearLayoutManager(activity, OrientationHelper.VERTICAL, false)

        adapter.setData(friendList)
        adapter.notifyDataSetChanged()
    }

    override fun showError() {
        Toast.makeText(activity, "Неизвестная ошибка", Toast.LENGTH_SHORT).show()
    }
}
