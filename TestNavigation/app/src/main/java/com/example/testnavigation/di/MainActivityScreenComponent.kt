package com.example.testnavigation.di

import com.example.testnavigation.MainActivity
import com.example.testnavigation.fragments.friends.di.FriendFragmentScreenComponent
import com.example.testnavigation.fragments.friends.di.FriendFragmentScreenModule
import com.example.testnavigation.fragments.photo.di.PhotoFragmentScreenComponent
import com.example.testnavigation.fragments.photo.di.PhotoFragmentScreenModule
import com.example.testnavigation.scope.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = arrayOf(MainActivityScreenModule::class))
interface MainActivityScreenComponent {

    fun inject(activity: MainActivity)

    fun plusAccount(module: FriendFragmentScreenModule): FriendFragmentScreenComponent
    fun plusSetting(module: PhotoFragmentScreenModule): PhotoFragmentScreenComponent
}