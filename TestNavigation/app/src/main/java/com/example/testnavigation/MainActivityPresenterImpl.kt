package com.example.testnavigation

import javax.inject.Inject

class MainActivityPresenterImpl
@Inject constructor(
    private val view: MainActivityView
) : MainActivityPresenter {
    override fun account() {
        view.showFriend()
    }

    override fun setting() {
        view.showFoto()
    }
}