package com.example.testnavigation.welcome_activity.di

import com.example.testnavigation.scope.ActivityScope
import com.example.testnavigation.welcome_activity.*
import dagger.Module
import dagger.Provides

@Module
class WelcomeActivityScreenModule(private val view: WelcomeActivityView) {


    @Provides
    @ActivityScope
    fun provideView(): WelcomeActivityView = view

    @Provides
    @ActivityScope
    fun providepresenter(presenter: WelcomeActivityPresenterImpl): WelcomeActivityPresenter = presenter

}