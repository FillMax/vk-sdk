@file:Suppress("DEPRECATION")

package com.example.testnavigation.app

import android.app.Application
import android.content.Context

class App : Application() {
    private lateinit var component: AppComponent


    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()

        component.injectApp(this)
    }

    fun getAppComponent(): AppComponent = component

    operator fun get(context: Context): App {
        return context.applicationContext as App
    }

}
