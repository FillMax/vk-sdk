package com.example.testnavigation.fragments.friends.di

import com.example.testnavigation.fragments.friends.FriendFragmentPresenter
import com.example.testnavigation.fragments.friends.FriendFragmentPresenterImpl
import com.example.testnavigation.fragments.friends.FriendFragmentView
import com.example.testnavigation.fragments.friends.interactor.FriendInteractor
import com.example.testnavigation.fragments.friends.interactor.FriendInteractorImpl
import com.example.testnavigation.scope.FragmentScope
import dagger.Module
import dagger.Provides

@Module
class FriendFragmentScreenModule(private val view: FriendFragmentView) {

    @Provides
    @FragmentScope
    fun provideView(): FriendFragmentView = view

    @Provides
    @FragmentScope
    fun providePresenter(presenter: FriendFragmentPresenterImpl): FriendFragmentPresenter = presenter

    @Provides
    @FragmentScope
    fun provideInteractor(interactor: FriendInteractorImpl): FriendInteractor = interactor
}