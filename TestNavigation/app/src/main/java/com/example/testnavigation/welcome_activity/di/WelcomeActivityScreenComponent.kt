package com.example.testnavigation.welcome_activity.di

import com.example.testnavigation.scope.ActivityScope
import com.example.testnavigation.welcome_activity.WelcomeActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = arrayOf(WelcomeActivityScreenModule::class))
interface WelcomeActivityScreenComponent {

    fun inject(activity: WelcomeActivity)
}