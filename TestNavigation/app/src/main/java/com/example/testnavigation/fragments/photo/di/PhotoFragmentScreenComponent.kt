package com.example.testnavigation.fragments.photo.di

import com.example.testnavigation.fragments.photo.PhotoFragment
import com.example.testnavigation.scope.FragmentScope
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = arrayOf(PhotoFragmentScreenModule::class))
interface PhotoFragmentScreenComponent {

    fun inject(fragment: PhotoFragment)
}