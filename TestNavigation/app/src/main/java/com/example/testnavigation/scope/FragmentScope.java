package com.example.testnavigation.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@FragmentScope
@Retention(RetentionPolicy.RUNTIME)
public @interface FragmentScope {
}
