package com.example.testnavigation.fragments.friends

import com.example.testnavigation.fragments.friends.model.VKUser

interface FriendFragmentView {
    fun showData(friendList: ArrayList<VKUser>)
    fun showError()
}