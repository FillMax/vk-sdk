package com.example.testnavigation.app

import com.example.testnavigation.di.MainActivityScreenComponent
import com.example.testnavigation.di.MainActivityScreenModule
import com.example.testnavigation.welcome_activity.di.WelcomeActivityScreenComponent
import com.example.testnavigation.welcome_activity.di.WelcomeActivityScreenModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    fun injectApp(app: App)

    fun plus(module: MainActivityScreenModule): MainActivityScreenComponent
    fun plus(module: WelcomeActivityScreenModule): WelcomeActivityScreenComponent
}