package com.example.testnavigation.app

import android.app.Application
import com.example.testnavigation.scope.ActivityScope
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideApp(): Application = app
}