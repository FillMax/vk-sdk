package com.example.testnavigation.welcome_activity

import android.content.Intent
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import javax.inject.Inject

class WelcomeActivityPresenterImpl
@Inject constructor(
    private val view: WelcomeActivityView
) : WelcomeActivityPresenter {

    private val error = "Произошла ошибка при авторизации"

    override fun welcome(isAccess: Boolean) {
        when (isAccess) {
            true -> view.start()
            else -> view.error(error)
        }
    }
}