package com.example.testnavigation.fragments.friends.interactor

import com.example.testnavigation.fragments.friends.model.VKUser
import io.reactivex.Observable

interface FriendInteractor {
    fun getListFriend(): Observable<ArrayList<VKUser>>
}