package com.example.testnavigation.welcome_activity


interface WelcomeActivityView {
    fun start()
    fun error(error: String)
}