package com.example.testnavigation.fragments.friends.interactor

import com.example.testnavigation.fragments.friends.model.VKUser
import com.example.testnavigation.requests.VKFriendsRequest
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKApiCallback
import com.vk.api.sdk.exceptions.VKApiExecutionException
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FriendInteractorImpl
@Inject constructor() : FriendInteractor {


    override fun getListFriend(): Observable<ArrayList<VKUser>> {

        return Observable.create<ArrayList<VKUser>> { emitter ->
            VK.execute(VKFriendsRequest(), object : VKApiCallback<ArrayList<VKUser>> {
                override fun success(result: ArrayList<VKUser>) {
                    emitter.onNext(result)
                    emitter.onComplete()
                }

                override fun fail(error: VKApiExecutionException) {
                    emitter.onError(error)
                }
            })
        }
    }
}
