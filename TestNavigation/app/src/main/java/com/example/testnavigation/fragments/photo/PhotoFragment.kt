package com.example.testnavigation.fragments.photo


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.os.Bundle

import com.example.testnavigation.base.BaseFragment
import javax.inject.Inject
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import com.example.testnavigation.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_photo.*
import java.net.URL


class PhotoFragment : BaseFragment(),
    PhotoFragmentView {

    @Inject
    lateinit var presenter: PhotoFragmentPresenter
    var url: String? = null

    override fun setupComponent() {
        (activity as PhotoFragmentInjector).inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_photo, container, false)
        return rootView


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        url = arguments?.getString(
            URL_USER,
            "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Antu_dialog-error.svg/1024px-Antu_dialog-error.svg.png"
        )

        openPhoto(url)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = {
                    photoUser.setImageBitmap(it)
                },
                onError = {
                    it.printStackTrace()
                }
            )
    }


    private fun openPhoto(tst: String?): Observable<Bitmap> {
        return Observable.create<Bitmap> { emitter ->
            try {
                val inputStream = URL(tst).openStream()
                emitter.onNext(BitmapFactory.decodeStream(inputStream))
            } catch (e: Exception) {
                e.printStackTrace()
                emitter.onError(e)
            }
        }
    }


    companion object {
        private const val URL_USER = "url"
        fun newInstance(url: String): PhotoFragment {
            val fragment = PhotoFragment()
            val args = Bundle()
            args.putString(URL_USER, url)
            fragment.arguments = args
            return fragment
        }
    }


}
