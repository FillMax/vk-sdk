package com.example.testnavigation.fragments.friends

interface FriendFragmentInjector {

    fun inject(fragment: FriendFragment)
}