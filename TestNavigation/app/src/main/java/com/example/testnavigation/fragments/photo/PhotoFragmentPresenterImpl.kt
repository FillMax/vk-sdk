package com.example.testnavigation.fragments.photo

import javax.inject.Inject

class PhotoFragmentPresenterImpl
@Inject constructor(private val view: PhotoFragmentView) : PhotoFragmentPresenter