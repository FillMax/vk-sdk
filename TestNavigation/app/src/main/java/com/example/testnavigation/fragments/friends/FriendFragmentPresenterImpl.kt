package com.example.testnavigation.fragments.friends

import com.example.testnavigation.fragments.friends.interactor.FriendInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FriendFragmentPresenterImpl
@Inject constructor(
    private val view: FriendFragmentView,
    private val interactor: FriendInteractor
) : FriendFragmentPresenter {

    override fun getListFriend() {

        interactor.getListFriend()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { data ->
                    view.showData(data)
                },
                onError = {
                    it.printStackTrace()
                }
            )
    }
}