package com.example.testnavigation.fragments.friends.di

import com.example.testnavigation.fragments.friends.FriendFragment
import com.example.testnavigation.scope.FragmentScope
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = arrayOf(FriendFragmentScreenModule::class))
interface FriendFragmentScreenComponent {

    fun inject(fragment: FriendFragment)
}